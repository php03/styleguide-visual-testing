FROM node:9 

WORKDIR /

COPY . .

RUN npm i

EXPOSE 8080

CMD ["npm", "start"]