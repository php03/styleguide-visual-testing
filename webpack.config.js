path = require('path');

module.exports = {
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    contentBase: path.resolve('dist'),
    publicPath: '/some/sub-path/',
    proxy: {
      '/some/sub-path/*': {
        target: 'http://localhost:8080/',
        pathRewrite: { '^/some/sub-path': '' },
      }
    }
  }
}