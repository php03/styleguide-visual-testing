
##Documentation
####Runing locally
`npm run start` /
`yarn start`
####Build
`npm run build` / `yarn build`
####Linting
`npm run lint:docs` / `yarn lint:docs`

##Styles
####Build css
`npm run css-compile-main` / `yarn css-compile-main`
####Linting
`npm run lint:styles` / `yarn lint styles`
