const Differencify = require('differencify');
const differencify = new Differencify({mismatchThreshold: 0});
let urlToTest = process.env.URL_TO_TEST;

describe('Differencify', () => {
  beforeAll(async () => {
    await differencify.launchBrowser({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
  });
  afterAll(async () => {
    await differencify.cleanup();
  });
  it('displays main page', async () => {
    const target = differencify.init({ chain: false });
    const page = await target.newPage();
    await page.goto(urlToTest);
    await page.setViewport({ width: 1600, height: 1200 });
    await page.waitFor(1000);
    const image = await page.screenshot();
    const result = await target.toMatchSnapshot(image);
    await page.close();
    expect(result).toEqual(true);
  }, 30000);

  it('displays subpage', async () => {
    const target = differencify.init({ chain: false });
    const page = await target.newPage();
    await page.goto(`${urlToTest}/subsite.html`);
    await page.setViewport({ width: 1600, height: 1200 });
    await page.waitFor(1000);
    const image = await page.screenshot();
    const result = await target.toMatchSnapshot(image);
    await page.close();
    expect(result).toEqual(true);
  }, 30000);
});
